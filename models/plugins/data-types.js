'use strict';

const _ = require('lodash');
const Sequelize = require('sequelize');
const DataTypes = {};

DataTypes.Number = (opt) => (_.merge({
    type: Sequelize.INTEGER,
}, opt));

DataTypes.IDReference = (model, key, opt) => (_.merge({
    type: Sequelize.INTEGER,
    references: {
        model: model,
        key: key || 'id',
        defferable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
    },
}, opt));

DataTypes.Date = (opt) => (_.merge({
    type: Sequelize.DATE,
}, opt));

DataTypes.BigInt = (opt) => (_.merge({
    type: Sequelize.BIGINT,
}, opt));

DataTypes.Boolean = (opt) => (_.merge({
    type: Sequelize.BOOLEAN,
}, opt));

DataTypes.String = (opt) => (_.merge({
    type: Sequelize.STRING,
}, opt));

DataTypes.Enum = (values, opt) => (_.merge({
    type: Sequelize.STRING,
    values: values,
}, opt));

module.exports = DataTypes;
