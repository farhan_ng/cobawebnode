var mongoose = require('mongoose');

//Skema Jual

var jualSkema = mongoose.Schema({
   nama:{
       type:String,
       required:true
   },
   stock:{
       type:Number,
       default:0
   },
   harga:{
       type:Number,
       default:0
   },
   create_date:{
       type:Date,
       default:Date.now
   }
},{collection:'jual'});

var Jual = module.exports = mongoose.model('jual', jualSkema);

//Get jual

module.exports.getJuals = function(callback, limit) {
    Jual.find(callback).limit(limit);
}
module.exports.getJualById = function(id, callback) {
    Jual.findById(id, callback);
}