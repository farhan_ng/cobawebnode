var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

//Skema User

var userSkema = mongoose.Schema({
    local: {
        username: String,
        password: String
    },
    facebook: {
        id : String,
        token : String,
        email : String,
        name : String
    },
    google: {
        id : String,
        token : String,
        email : String,
        name : String
    }
},{collection:'User'});

userSkema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(9));
}
userSkema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
}

module.exports = mongoose.model('User', userSkema);



