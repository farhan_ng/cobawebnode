var mongoose = require('mongoose');

//Skema Genre

var genreSkema = mongoose.Schema({
   nama:{
       type:String,
       required:true
   },
   create_date:{
       type:Date,
       default:Date.now
   }
},{collection:'genre'});

var Genre = module.exports = mongoose.model('genre', genreSkema);

//Get Genre

module.exports.getGenres = function(callback, limit) {
    Genre.find(callback).limit(limit);
}

//Add Genre
module.exports.addGenre = function(genre, callback) {
    Genre.create(genre, callback);
}

//Update Genre
module.exports.updateGenre = function(id, genre, options, callback) {
    var query = {_id: id};
    var update = {
        nama : genre.nama
    }
    Genre.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteGenre = function(id, callback) {
    var query = {_id: id};
    Genre.remove(query, callback);
}