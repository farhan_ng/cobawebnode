var Jual = require('../models/jual');
var Genre = require('../models/genre');
var User = require('../models/user');

module.exports = function (app, passport) {

    app.get('/', function(req, res) {
        res.render('landing');

    });

    app.get('/barang', function(req, res){
        Jual.getJuals(function(err, juals){
            if (err) {
                throw err;
            }
            res.json(juals);
        });
    });

    app.get('/barang/:_id', function(req, res){
        Jual.getJualById(req.params._id, function(err, jual){
            var status = '';
            var datameta
            if(jual) {
                status = "ok";
                datameta = "1";

            } else {
                status = "Data not Found";
                datameta = err;
            }

            var data = {
                meta : {
                    'status' : status,
                    'query' : datameta
                },
                data : jual
            }
            res.json(data);
        });
    });

    app.get('/genre', function(req, res){
        Genre.getGenres(function(err, genres){
            if (err) {
                throw err;
            }
            res.json(genres);
        });
    });

    app.post('/genre', function(req, res){
        var genre = req.body;
        Genre.addGenre(genre, function(err, genre){
            if(err) {
                throw err;
            }
            res.json(genre);
        });
    });

    app.put('/genre/:_id', function(req, res){
        var id = req.params._id;
        var genre = req.body;
        Genre.updateGenre(id, genre, {},function(err, genre){
            if(err) {
                throw err;
            }
            res.json(genre);
        });
    });

    app.delete('/genre/:_id', function(req, res){
        var id = req.params._id;
        Genre.deleteGenre(id, function(err, genre){
            if(err) {
                throw err;
            }
            res.json(genre);
        });
    });

//AUTH
    app.get('/signup', function(req, res) {
        res.render('signup', {title : 'NODE APPS',
            message : req.flash('signupMessage') });
    });
    app.get('/login', function(req, res) {
        res.render('login', {title : 'NODE APPS',
            message : req.flash('loginMessage') });
    });

    app.post('/signup', passport.authenticate('local-signup', { failureRedirect: '/signup' }),
        function(req, res) {
            res.redirect('/');
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile',
        failureRedirect: '/login',
        failureFlash : true
    }));

    app.get('/profile', isLoggedIn ,function(req, res) {
        res.render('profile', { user: req.user });
    });

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });


    app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email']}),
        function(req, res){
    });

    app.get('/auth/google', passport.authenticate('google', { scope: [ 'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email']}),
        function(req, res){
        });

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', { successRedirect: '/profile',
            profileFields: ['id', 'name','picture.type(large)', 'emails', 'displayName', 'about', 'gender'],
            failureRedirect: '/' }));

    app.get('/auth/google/callback',
        passport.authenticate('google', { successRedirect: '/profile',
            failureRedirect: '/' }));
};

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}