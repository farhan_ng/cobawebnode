var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-auth').Strategy;


var User = require('../models/user');
var configAuth = require('./auth');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null,user._id);
    });
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    //PASSPORT BUAT SIGN UP
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField:  'password',
            passReqToCallback: true
    },  function (req, email, password, done) {
        console.log('nyampe passport');
            process.nextTick(function(){
                User.findOne({'local.username': email},function(err,user){
                   if(err) return done(err);
                   if(user) {
                       return done(null, false, req.flash('signupMessage', "Email Already"));
                       console.log('email already');
                   } else {
                       var newUser = new User(req.body);
                       newUser.local.username = email;
                       newUser.local.password = newUser.generateHash(password);

                       console.log(newUser.local.username);
                       console.log(newUser.local.password);

                       newUser.save(function(err){
                           console.log(newUser);
                           if(err) {
                               throw err;
                           }
                           return done(null, newUser);
                       });
                   }
                });

            });
        }

    ));


    //PASSPORT BUAT LOGIN
    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function (req, email, password, done) {
        process.nextTick(function () {
            User.findOne({ 'local.username' : email }, function (err, user) {
                if(err)
                    return done(err);
                if(!user)
                    return done(null, false, req.flash('loginMessage','User not Found'));
                if(!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage','Password Wrong'));

                    return done(null, user);

            });
        });
    }));


    passport.use(new FacebookStrategy({
            clientID: configAuth.facebookAuth.clientID,
            clientSecret: configAuth.facebookAuth.clientSecret,
            callbackURL: configAuth.facebookAuth.callbackURL,
            profileFields: ['id', 'displayName', 'link', 'photos', 'emails']
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                User.findOne({'facebook.id' : profile.id }, function (err, user) {
                    if(err)
                        return done(err);
                    if(user)
                            return done(null, user);

                    else {
                        var newUser = new User();
                        newUser.facebook.id = profile.id;
                        newUser.facebook.token = accessToken;
                        newUser.facebook.name = profile.displayName;
                        newUser.facebook.email = profile.emails;

                        newUser.save(function (err) {
                            if(err)
                                throw err;

                        });
                        return done(null, newUser);
                    }
                });
            });
        }
    ));

    passport.use(new GoogleStrategy({
            clientId: configAuth.googleAuth.clientID,
            clientSecret: configAuth.googleAuth.clientSecret,
            callbackURL: configAuth.googleAuth.callbackURL,
            passReqToCallBack: true
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                User.findOne({'google.id' : profile.id }, function (err, user) {
                    if(err)
                        return done(err);
                    if(user)
                        return done(null, user);

                    else {
                        var newUser = new User();
                        newUser.google.id = profile.id;
                        newUser.google.token = accessToken;
                        newUser.google.name = profile.displayName;
                        newUser.google.email = profile.emails[0].value;

                        newUser.save(function (err) {
                            if(err)
                                throw err;


                        });
                        return done(null, newUser);
                    }
                });
            });
        }
    ));

}