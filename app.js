//Init
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;

var session = require('express-session');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var fs = require('fs');
var swig = require('swig');
var swig = new swig.Swig();
var passport = require('passport');
var flash = require('connect-flash');
var router = './routes/api';
var logger = require('morgan');

//Use Package
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({secret : 'sekretkodeaku',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());



//Set Express Render Swig
app.engine('html', swig.renderFile);
app.set('view engine', 'html');


//Connect to mongoose
mongoose.connect('mongodb://localhost/barang');
var db = mongoose.connection;
require('./config/passport')(passport);

//Router APPS
require(router)(app, passport);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

//Listeninng PORT
app.listen(port, function () {
    console.log('Work on Port 3000');
});
