# NOTE
This is Example Project for Web Back-end Developers 

# INSTALLATION

```
npm install
```

# RUN PROJECT
* NODE
```
node app
```
* WEB BROWSER
```
http://localhost:3000
```


# FEATURE
* Basic RESTful API Node JS using ExpressJS and MongoDB
* HTML Parse using Swig Module
* Session Storing Message using connect-flash module
* Basic Auth System Login Using Passport JS
* Bcrypt Password Generating
* OAuht Facebook API using passport-facebook module
* OAuth2 Google API using passport-google-auth
